INTRODUCTION
------------

This is the sendy module for Drupal 7. Sendy (http://sendy.co/?ref=THB3d)
sends newsletters via Amazon SES for a fraction of the cost of MailChimp or
Campaign Monitor. This module 1) adds a block to capture emails of anonymous
users, and 2) adds a field to the user profile to allow registered users to
opt in or out of a registered users email list.

If you don't already own Sendy and plan to use this module, please use our
affiliate link to buy it: http://sendy.co/?ref=THB3d. Full disclosure, we
receive a commission of $12 with each purchase of Sendy.

I hope this module is useful, and thank you.


REQUIREMENTS
------------

This module requires the following modules:
 * List (core)


INSTALLATION
------------

 * Install as you would normally install a contributed Drupal module. See:
   https://drupal.org/documentation/install/modules-themes/modules-7
   for further information.

 * Requires access to a live instance of Sendy (http://sendy.co/?ref=THB3d).
   Sendy installation docs are here: http://sendy.co/get-started?ref=THB3d.

 * Sendy module will add profile_sendy_subscribed field to user data form.
   If you uninstall Sendy module profile_sendy_subscribed and user opt-in
   data will be deleted from Drupal.

 * Sendy module will add Sendy block to capture anonymous user email
   addresses.


CONFIGURATION
-------------

Sendy module configuration available at Admin->Configuration->People->Sendy
(/admin/config/people/sendy).

 * URL of your Sendy install - Enter the URL of your sendy install
   without the trailing slash (e.g. http://sendy.yourdomain.com).

 * Anonymous List ID - Enter the list ID of the list you would like
   emails submitted via the Sendy module block to be added to. The List ID can
   be found on the "Subscriber lists" page of your Sendy install.

 * Registered Users List ID - Enter the list ID of the list you
   would like registered users who opt-in or opt-out via their user profile
   to be added and removed from. List ID can be found on the "Subscriber
   lists" page of your Sendy install.

 * Sendy Block Blurb - Customizable blurb of text that appears above the form
   on the Sendy module block.


MAINTAINERS
-----------

Current maintainers:
 * Joseph Lanza (guinness74) - https://drupal.org/user/2894173

This project has been sponsored by:
 * ORANGE AND MAROON MEDIA, LLC
   http://www.orangeandmaroonmedia.com
