<?php

/**
 * @file
 * A block and profile module that integrates Drupal with Sendy.
 */

/**
 * Implements hook_help().
 */
function sendy_help($path, $arg) {
  switch ($path) {
    case "admin/help#sendy":
      return '<p>' . t("Integrates Drupal with Sendy (http://sendy.co/?ref=THB3d) email newsletter service. Provides a block for anonymous email signups and automatically adds new website accounts to registered users email list.") . '</p>';
  }
}

/**
 * Implements hook_block_info().
 */
function sendy_block_info() {
  $blocks['sendy'] = array(
    // The name that will appear in the block list.
    'info' => t('Sendy'),
    // Default setting.
    'cache' => DRUPAL_CACHE_PER_ROLE,
  );
  return $blocks;
}

/**
 * Implements hook_block_view().
 *
 * Prepares the contents of the block.
 */
function sendy_block_view($delta = '') {
  switch ($delta) {
    case 'sendy':
      $block['subject'] = t('Email Newsletter');

      // Url and anonymous list id need to be set before
      // the Sendy block will work.
      $sendy_url               = trim(check_url(variable_get('sendy_url', '')));
      $sendy_anonymous_list_id = trim(variable_get('sendy_anonymous_list_id', ''));

      if ($sendy_url == '' || $sendy_anonymous_list_id == '') {
        $block['content']  = '<p>' . l(t('You must set your Sendy URL and Anonymous List ID before the Sendy block will work.'), 'admin/config/people/sendy') . '</p>';
      }
      elseif (user_access('access content')) {
        $form = drupal_get_form('sendy_block_register_form');

        $block['content']  = '';

        $sendy_block_blurb = trim(variable_get('sendy_block_blurb', ''));

        if ($sendy_block_blurb != '') {
          $block['content'] .= '<p id="sendy-block-blurb">' . t(check_plain($sendy_block_blurb)) . '</p>';
        }

        $block['content'] .= drupal_render($form);
        $block['content'] .= '<div id="sendy-block-form-result-wrapper"></div>';
      }

      return $block;
  }
}

/**
 * Form constructor for the sendy block register form.
 *
 * @see sendy_block_register_form_validate()
 * @see sendy_block_register_form_submit()
 *
 * @ingroup forms
 */
function sendy_block_register_form($form, &$form_state) {
  $form['email'] = array(
    '#type' => 'textfield',
    '#attributes' => array(
      'placeholder' => 'email@address.com',
    ),
    '#size' => 30,
    '#maxlength' => 128,
    '#required' => TRUE,
  );

  $form['subscribe'] = array(
    '#type' => 'button',
    '#value' => t('Subscribe'),
    '#ajax' => array(
      'callback' => 'sendy_block_register_form_submit',
      'wrapper' => 'sendy-block-form-result-wrapper',
      'method' => 'replace',
      'effect' => 'none',
    ),
  );

  return $form;
}

/**
 * Form validation handler for sendy_block_register_form().
 *
 * @see sendy_block_register_form_submit()
 */
function sendy_block_register_form_validate($form, &$form_state) {
}

/**
 * Form submission handler for sendy_block_register_form().
 *
 * @see sendy_block_register_form_validate()
 */
function sendy_block_register_form_submit($form, &$form_state) {
  $prefix = '<div id="sendy-block-form-result-wrapper">';
  $suffix = '</div>';

  if (valid_email_address($form_state['values']['email'])) {
    $message = '';
    $sendy_anonymous_list_id = variable_get('sendy_anonymous_list_id', '');

    sendy_subscribe($sendy_anonymous_list_id, $form_state['values']['email'], '', $message);

    return $prefix . $message . $suffix;
  }
  else {
    return $prefix . t('Please provide a valid email address.') . $suffix;
  }
}

/**
 * Subscribes a user to an email list via the Sendy API.
 *
 * @param string $list
 *   Sendy list to subscribe to.
 * @param string $email
 *   Email address to subscribe to the list.
 * @param string $name
 *   Name associated with email.
 * @param string $message
 *   Human readable result of the API request.
 *
 * @return bool
 *   TRUE if user was subscribed, otherwise FALSE
 */
function sendy_subscribe($list, $email, $name, &$message) {
  $sendy_url = check_url(variable_get('sendy_url', ''));

  $request_options = array(
    'headers' => array(
      'Content-type' => 'application/x-www-form-urlencoded',
    ),
    'method'  => 'POST',
    'data'    => drupal_http_build_query(array(
      'list'    => $list,
      'email'   => $email,
      'name'    => $name,
      'boolean' => 'true',
    )),
    'timeout' => 60,
  );

  $result  = drupal_http_request($sendy_url . '/subscribe', $request_options);
  $success = TRUE;

  if ($result->data === '1') {
    $message = t('Successfully subscribed!');
  }
  elseif ($result->data == 'Already subscribed.') {
    $success = FALSE;
    $message = t('Already subscribed.');
  }
  else {
    $watchdog_message = 'Unable to subscribe %email to %list: %result';
    $watchdog_variables = array(
      '%result' => var_export($result, TRUE),
      '%email' => $email,
      '%list' => $list,
    );

    watchdog('sendy', $watchdog_message, $watchdog_variables, WATCHDOG_ERROR);

    $success = FALSE;
    $message = t('Error, please contact administrator.');
  }

  return $success;
}

/**
 * Unsubscribes a user from an email list via the Sendy API.
 *
 * @param string $list
 *   Sendy list to unsubscribe from.
 * @param string $email
 *   Email address to unsubscribe from the list.
 * @param string $message
 *   Human readable result of the API request.
 *
 * @return bool
 *   TRUE if user was unsubscribed, otherwise FALSE
 */
function sendy_unsubscribe($list, $email, &$message) {
  $sendy_url = check_url(variable_get('sendy_url', ''));

  $request_options = array(
    'headers' => array(
      'Content-type' => 'application/x-www-form-urlencoded',
    ),
    'method'  => 'POST',
    'data'    => drupal_http_build_query(array(
      'list'    => $list,
      'email'   => $email,
      'boolean' => 'true',
    )),
    'timeout' => 60,
  );

  $result  = drupal_http_request($sendy_url . '/unsubscribe', $request_options);
  $success = TRUE;

  if ($result->data === '1') {
    $message = t('Successfully unsubscribed!');
  }
  elseif ($result->data == 'Invalid email address.') {
    $success = FALSE;
    $message = t('Invalid email address.');
  }
  else {
    $watchdog_message = 'Unable to unsubscribe %email from %list: %result';
    $watchdog_variables = array(
      '%result' => var_export($result, TRUE),
      '%email' => $email,
      '%list' => $list,
    );

    watchdog('sendy', $watchdog_message, $watchdog_variables, WATCHDOG_ERROR);

    $success = FALSE;
    $message = t('Error, please contact administrator.');
  }

  return $success;
}

/**
 * Implements hook_menu().
 */
function sendy_menu() {
  $items = array();

  $items['admin/config/people/sendy'] = array(
    'title' => 'Sendy',
    'description' => 'Configuration for Sendy module',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('sendy_form'),
    'access arguments' => array('administer site configuration'),
    'type' => MENU_NORMAL_ITEM,
  );

  return $items;
}

/**
 * Page callback: Sendy settings.
 *
 * @see sendy_menu()
 */
function sendy_form($form, &$form_state) {
  $form['sendy_url'] = array(
    '#type' => 'textfield',
    '#title' => t('URL of your Sendy install'),
    '#default_value' => variable_get('sendy_url', ''),
    '#size' => 50,
    '#maxlength' => 256,
    '#description' => t('The URL of your Sendy install without the trailing slash, for example: http://sendy.yourdomain.com.'),
    '#required' => TRUE,
    '#attributes' => array(
      'placeholder' => 'http://sendy.yourdomain.com',
    ),
  );

  $form['sendy_anonymous_list_id'] = array(
    '#type' => 'textfield',
    '#title' => t('Anonymous List ID'),
    '#default_value' => variable_get('sendy_anonymous_list_id', ''),
    '#size' => 24,
    '#maxlength' => 50,
    '#description' => t('List ID for anonymous emails captured from the Sendy block.'),
    '#required' => TRUE,
  );

  $form['sendy_registered_users_list_id'] = array(
    '#type' => 'textfield',
    '#title' => t('Registered Users List ID'),
    '#default_value' => variable_get('sendy_registered_users_list_id', ''),
    '#size' => 24,
    '#maxlength' => 50,
    '#description' => t('List ID for registered users that can opt-in or opt-out via their user profile.'),
    '#required' => TRUE,
  );

  $form['sendy_block_blurb'] = array(
    '#type' => 'textarea',
    '#title' => t('Sendy Block Blurb'),
    '#default_value' => variable_get('sendy_block_blurb', ''),
    '#description' => t('Snippet of text displayed above the subscribe form.'),
    '#required' => FALSE,
  );

  return system_settings_form($form);
}

/**
 * Implements hook_form_alter().
 *
 * @see sendy_user_profile_form_submit_handler()
 */
function sendy_form_alter(&$form, &$form_state, $form_id) {
  if ($form_id == 'user_profile_form' || $form_id == 'user_register_form') {
    $form['#submit'][] = 'sendy_user_profile_form_submit_handler';
  }
}

/**
 * Submit form handler for sendy_form_alter().
 */
function sendy_user_profile_form_submit_handler($form, &$form_state) {
  $uid        = $form['#user']->uid;
  $mail       = $form['#user']->mail;
  $name       = $form['#user']->name;
  $subscribed = $form['#user']->profile_sendy_subscribed[LANGUAGE_NONE][0]['value'];
  $sendy_registered_users_list_id = variable_get('sendy_registered_users_list_id', '');

  // Not only can users subscribe and unsubscribe, but they
  // can change their email address. Only the current email
  // needs to be subscribed. Although, all emails will be tracked.
  // Check `sendy` to see if mail + uid combination is already
  // subscribed. If not, unsubscribe last mail + uid combination,
  // update `sendy` table and subscribe this one.
  if ($subscribed) {
    $sendy_subscribed = db_query_range('SELECT subscribed FROM {sendy} WHERE uid = :uid AND mail = :mail ORDER BY subscribe_date DESC', 0, 1, array(':uid' => $uid, ':mail' => $mail))->fetchField();

    // Is this mail + uid combination subscribed?
    if (!$sendy_subscribed) {

      // Fetch last subscribed row with alternate email.
      $row = db_query_range('SELECT * FROM {sendy} WHERE uid = :uid AND mail <> :mail AND subscribed = 1 ORDER BY subscribe_date DESC ', 0, 1, array(':uid' => $uid, ':mail' => $mail))->fetchAssoc();

      // There's a previous email address that needs to be unsubscribed.
      if ($row) {

        // Mark `sendy` unsubscribed.
        $num_updated = db_update('sendy')
          ->fields(array(
            'subscribed' => 0,
          ))
          ->condition('sid', $row['sid'])
          ->execute();

        if (!$num_updated) {
          watchdog('sendy', 'Unable to update `sendy`.`unsubscribed` sid: %sid.', array('%sid' => $row['sid']), WATCHDOG_ERROR);
        }

        // Unsubscribe from list.
        $message = '';
        sendy_unsubscribe($sendy_registered_users_list_id, $row['mail'], $message);
      }

      // Insert new row in DB.
      $data = array(
        'uid' => $uid,
        'subscribed' => 1,
        'mail' => $mail,
        'subscribe_date' => time(),
      );

      $sid = db_insert('sendy')->fields($data)->execute();

      // Error check.
      if (!$sid) {
        watchdog('sendy', 'Unable to insert new row to `sendy`: %data.', array('%data' => var_export($data, TRUE)), WATCHDOG_ERROR);
      }

      // Subscribe new mail.
      $message = '';
      sendy_subscribe($sendy_registered_users_list_id, $mail, $name, $message);
    }
  }
  // Unsubscribe mail, uid combination and update `sendy` table.
  else {
    // Fetch last subscribed row.
    $row = db_query_range('SELECT * FROM {sendy} WHERE uid = :uid AND subscribed = 1 ORDER BY subscribe_date DESC', 0, 1, array(':uid' => $uid))->fetchAssoc();

    // The last email for the uid should be the
    // only one subscribed. Get its info and
    // unsubscribe. We can't trust $mail because
    // it could have been changed by the user.
    if ($row) {
      // Mark `sendy` unsubscribed.
      $num_updated = db_update('sendy')
        ->fields(array(
          'subscribed' => 0,
        ))
        ->condition('sid', $row['sid'])
        ->execute();

      if (!$num_updated) {
        watchdog('sendy', 'Unable to update `sendy`.`unsubscribed` mail: %mail, uid: %uid.', array('%mail' => $mail, '%uid' => $uid), WATCHDOG_ERROR);
      }

      // Unsubscribe from list.
      $message = '';
      sendy_unsubscribe($sendy_registered_users_list_id, $row['mail'], $message);
    }
  }
}

/**
 * Implements hook_user_view().
 */
function sendy_user_view($account, $view_mode, $langcode) {
  // Remove opt-in status from user profile display.
  if ($view_mode == 'full') {
    unset($account->content['profile_sendy_subscribed']);
  }
}
